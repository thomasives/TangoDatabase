# - Try to find MySQL.
# Once done this will define:
# MYSQL_FOUND			- If false, do not try to use MySQL.
# MYSQL_INCLUDE_DIRS	- Where to find mysql.h, etc.
# MYSQL_LIBRARIES		- The libraries to link against.
#

if( WIN32 )
    find_path( MYSQL_INCLUDE_DIR
            NAMES "mysql.h"
            PATHS "$ENV{PROGRAMFILES}/MySQL/*/include"
            "$ENV{PROGRAMFILES(x86)}/MySQL/*/include"
            "$ENV{SYSTEMDRIVE}/MySQL/*/include" )

    find_library( MYSQL_LIBRARY
            NAMES "mysqlclient" "mysqlclient_r"
            PATHS "$ENV{PROGRAMFILES}/MySQL/*/lib"
            "$ENV{PROGRAMFILES(x86)}/MySQL/*/lib"
            "$ENV{SYSTEMDRIVE}/MySQL/*/lib" )
else()
    find_path( MYSQL_INCLUDE_DIR
            NAMES "mysql.h"
            PATHS "/usr/include/mysql"
            "/usr/include/mariadb"
            "/usr/local/include/mysql"
            "/usr/mysql/include/mysql" )

    find_library( MYSQL_LIBRARY
            NAMES "mysqlclient" "mysqlclient_r" "mariadbclient"
            PATHS "/lib/mysql"
            "/lib64/mysql"
            "/usr/lib/mysql"
            "/usr/lib/mariadb"
            "/usr/lib64/mysql"
            "/usr/local/lib/mysql"
            "/usr/local/lib64/mysql"
            "/usr/mysql/lib/mysql"
            "/usr/mysql/lib64/mysql" )
endif()

if(NOT DEFINED DB_CLIENT_COMPILE)
  try_run(DB_CLIENT_COMPILE
          DB_CLIENT_RUN
          ${CMAKE_CURRENT_BINARY_DIR}
          ${CMAKE_CURRENT_SOURCE_DIR}/configure/test_db_client.cpp
          COMPILE_DEFINITIONS "-I ${MYSQL_INCLUDE_DIR}"
          LINK_LIBRARIES "${MYSQL_LIBRARY}"
          COMPILE_OUTPUT_VARIABLE DB_CLIENT_COMPILE_OUTPUT
          RUN_OUTPUT_VARIABLE DB_CLIENT_VERSION)

  set(msg "Check if we can compile a simple database client program: ${DB_CLIENT_COMPILE}")

  if(NOT DB_CLIENT_COMPILE)
    message(STATUS ${msg})
  else()
    message(FATAL_ERROR ${msg} "\n\nBuild output:\n" ${DB_CLIENT_COMPILE_OUTPUT})
  endif()

  string(STRIP ${DB_CLIENT_VERSION} DB_CLIENT_VERSION)
  set(TDB_DB_CLIENT_VERSION ${DB_CLIENT_VERSION} CACHE INTERNAL "database client version")

  message("Output: ${TDB_DB_CLIENT_VERSION}")
endif()

# handle the QUIETLY and REQUIRED arguments and set MYSQL_FOUND to TRUE if
# all listed variables are TRUE
include( FindPackageHandleStandardArgs )

find_package_handle_standard_args( MYSQL DEFAULT_MSG MYSQL_LIBRARY MYSQL_INCLUDE_DIR)

set( MYSQL_INCLUDE_DIRS ${MYSQL_INCLUDE_DIR} )
set( MYSQL_LIBRARIES ${MYSQL_LIBRARY} )

mark_as_advanced( MYSQL_INCLUDE_DIR MYSQL_LIBRARY )
